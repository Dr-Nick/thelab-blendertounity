﻿using UnityEngine;

public class CharacterController : MonoBehaviour
{

    public float speed = 10.0f;
    private float m_translation;
    private float m_strafe;
    private Rigidbody m_rigidBody;
    private Transform m_transform;

    // Use this for initialization
    void Start()
    {
        // turn off the cursor
        Cursor.lockState = CursorLockMode.Locked;
        m_rigidBody = GetComponent<Rigidbody>();
        m_transform = transform;
    }

    // Update is called once per frame
    void Update()
    {
        // Input.GetAxis() is used to get the user's input
        // You can furthor set it on Unity. (Edit, Project Settings, Input)
        m_translation = Input.GetAxis("Vertical");
        m_strafe = Input.GetAxis("Horizontal");
        //transform.Translate(m_strafe, 0, m_translation);

        if (Input.GetKeyDown("escape"))
        {
            // turn on the cursor
            if (Cursor.lockState == CursorLockMode.Locked) 
            {
                Cursor.lockState = CursorLockMode.None;
            }
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
            }

        }
    }

    private void FixedUpdate()
    {
        Vector3 forward = m_transform.forward * m_translation;
        Vector3 strafe = m_transform.right * m_strafe;

        Vector3 movement = forward + strafe;
        movement *= speed * Time.fixedDeltaTime;

        //movement.x *= m_strafe;
        //movement.z *= m_translation;
        //movement *= speed * Time.fixedDeltaTime;

        m_rigidBody.position += movement;
    }



}